#!/bin/env python3

import sys
from typing import List, Tuple
from colorama import Fore, Back, Style
from random import randrange
from getchlib import getkey
import signal

#
# game configuration
# (need to be moved in a configuration file or to be taken from cmdline)
#
FILE_PATH = r'./dicts/french.dict'
MAX_TURN = 6
MIN_SIZE = 6
MAX_SIZE = 9
#
#

signal.signal(signal.SIGINT, signal.SIG_IGN)

KEY_UP = "\x1B[A"
KEY_DOWN = "\x1B[B"
KEY_RIGHT = "\x1B[C"
KEY_LEFT = "\x1B[D"
KEY_BACKSPACE = "\x7f"

PROMPT = "$> "

#
# funcs
#

CMD_MAP = {}


def cmd_help() -> None:
    for cmd, func in CMD_MAP.items():
        print("%s\t\t%s" % (cmd, func.description))


CMD_MAP["/help"] = cmd_help
cmd_help.description = "Display this help"


def cmd_abc() -> None:
    for i in range(len(cmd_abc.alphabet)):
        if cmd_abc.alphabet[i] == 1:
            print(Fore.BLACK + Style.BRIGHT, end='')
        elif cmd_abc.alphabet[i] == 2:
            print(Fore.YELLOW, end='')
        elif cmd_abc.alphabet[i] == 3:
            print(Back.GREEN, end='')
        print(chr(ord('A') + i), end='')
        print(Style.RESET_ALL, end='')
    print()


CMD_MAP["/abc"] = cmd_abc
cmd_abc.description = "Display alphabet to indicate unused/inplace/matching/non-matching letters"
# will need a small refactor to work with russian
cmd_abc.alphabet = [0] * 26
# 0 -> never used
# 1 -> non matching
# 2 -> matching
# 3 -> inplace


def cmd_inplace() -> None:
    print(' ' * len(PROMPT), end='')
    for i in range(len(cmd_inplace.word)):
        if cmd_inplace.word[i] != '_':
            print(Back.GREEN, end='')
        print(cmd_inplace.word[i], end='')
        print(Style.RESET_ALL, end='')
    print()


CMD_MAP["/inp"] = cmd_inplace
cmd_inplace.description = "Display all the letters inplace"
cmd_inplace.word = []


def cmd_history() -> None:
    for i in range(len(cmd_history.words)):
        print_word(cmd_history.words[i], cmd_history.matrices[i])


CMD_MAP["/his"] = cmd_history
cmd_history.description = "Display all the try words"
cmd_history.words = []
cmd_history.matrices = []


def check_line(line: str, dic: List[str], word: str, word_matrix: List[int]) -> bool:
    if len(line) == 0:
        return False
    if line[0] == '/':
        if line.lower() in CMD_MAP:
            CMD_MAP[line.lower()]()
        else:
            print("Command not found. Only the following commands are available :")
            cmd_help()
        return False
    if len(line) != len(word):
        print("Size doesn't match.")
        print_word(word, word_matrix)
        return False
    if line.upper() not in dic:
        print("Word not found in dictionary.")
        print_word(word, word_matrix)
        return False
    return True


def check_word(word: str, goal: str) -> Tuple[List[int], bool]:
    word = list(word.upper())
    goal = list(goal.upper())
    win = True
    word_matrix = []
    for i in range(len(word)):
        if word[i] == goal[i]:
            if cmd_abc.alphabet[ord(word[i]) - ord('A')] < 3:
                cmd_abc.alphabet[ord(word[i]) - ord('A')] = 3
            cmd_inplace.word[i] = word[i]
            word_matrix.append(2)
            word[i] = 0
            goal[i] = 0
        else:
            win = False
            word_matrix.append(0)
            if cmd_abc.alphabet[ord(word[i]) - ord('A')] < 1:
                cmd_abc.alphabet[ord(word[i]) - ord('A')] = 1

    if not win:
        for i in range(len(word)):
            if word[i] != 0 and word[i] in goal and word[:i+1].count(word[i]) <= goal.count(word[i]):
                word_matrix[i] = 1
                if cmd_abc.alphabet[ord(word[i]) - ord('A')] < 2:
                    cmd_abc.alphabet[ord(word[i]) - ord('A')] = 2

    return word_matrix, win


def print_word(word: str, matrix: List[int]) -> None:
    print(' ' * len(PROMPT), end='')
    for i in range(len(word)):
        if matrix[i] == 1:
            print(Fore.YELLOW, end='')
        elif matrix[i] == 2:
            print(Back.GREEN, end='')
        print(word[i], end='')
        print(Style.RESET_ALL, end='')
    print()


def replace_line(old_line: str, new_line: str, old_cursor: int, new_cursor: int) -> None:
    blank = ""
    if len(new_line) < len(old_line):
        blank = " " * (len(old_line) - len(new_line))
    print(KEY_LEFT * old_cursor + new_line + blank + KEY_LEFT * (len(new_line) - new_cursor + len(blank)), end='', flush=True)


def key_filter(key, cursor: int, line: str, history: List[str], history_cursor: int) -> Tuple[bool, int, str, int]:
    is_input = True
    if key == "CTRL-D":
        print()
        exit(0)
    if key == KEY_UP or key == KEY_DOWN:
        is_input = False
        new_line = None
        if key == KEY_UP and history_cursor < len(history):
            if history_cursor == 0:
                key_filter.actual_line = line
            history_cursor += 1
            new_line = history[- history_cursor]
        elif key == KEY_DOWN and history_cursor > 0:
            history_cursor -= 1
            if history_cursor == 0:
                new_line = key_filter.actual_line
            else:
                new_line = history[- history_cursor]
        if new_line is not None:
            replace_line(line, new_line, cursor, len(new_line))
            line = new_line
            cursor = len(new_line)
    elif key == KEY_LEFT:
        is_input = False
        if cursor > 0:
            cursor -= 1
            print(KEY_LEFT, end='', flush=True)
    elif key == KEY_RIGHT:
        is_input = False
        if cursor < len(line):
            cursor += 1
            print(KEY_RIGHT, end='', flush=True)
    elif key == KEY_BACKSPACE:
        is_input = False
        if cursor > 0:
            new_line = line[:cursor - 1] + line[cursor:]
            replace_line(line, new_line, cursor, cursor - 1)
            line = new_line
            cursor -= 1
    return is_input, cursor, line, history_cursor


def get_input() -> str:
    cursor = 0
    history_cursor = 0
    print(PROMPT, end='', flush=True)

    line = ""
    key = getkey()
    while key != '\n':
        is_input, cursor, line, history_cursor = key_filter(key, cursor, line, get_input.history, history_cursor)
        if is_input:
            if cursor < len(line):
                new_line = line[:cursor] + key + line[cursor:]
                replace_line(line, new_line, cursor, cursor + 1)
                line = new_line
            else:
                print(key, end='', flush=True)
                line += key
            cursor += 1
        key = getkey()
    if len(line) > 0 and (len(get_input.history) == 0 or get_input.history[-1] != line):
        get_input.history.append(line)
    print()
    return line.strip().upper()


# init static var
get_input.history = []


#
# main
#
def main():
    my_dict = []

    with open(FILE_PATH, 'rt') as dict_file:
        for line in dict_file:
            line = line[:len(line) - 1]
            if MIN_SIZE <= len(line) <= MAX_SIZE:
                my_dict.append(line.upper())

    word_pos = randrange(len(my_dict))
    word = list(my_dict[word_pos])
    word_matrix = [0] * len(word)
    word_matrix[0] = 2
    cmd_inplace.word.append(word[0])

    for i in range(1, len(word)):
        word[i] = '_'
        cmd_inplace.word.append('_')

    win = False

    lasted = MAX_TURN

    for turn in range(1, MAX_TURN + 1):

        if win:
            lasted = turn - 1
            break

        print("Turn %i :" % turn)

        print_word(word, word_matrix)
        line = get_input()

        while not check_line(line, my_dict, word, word_matrix):
            line = get_input()

        word = line
        word_matrix, win = check_word(word, my_dict[word_pos])
        cmd_history.words.append(word)
        cmd_history.matrices.append(word_matrix)
        print()

    if not win:
        print("You failed! Word was %s." % my_dict[word_pos])
    else:
        print_word(word, word_matrix)
        print("Resolved in %i turns." % lasted)


if __name__ == '__main__':
    main()
